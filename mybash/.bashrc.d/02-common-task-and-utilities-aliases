#!/usr/bin/env bash

# common task and utilities
#
# 'cp', 'mv', 'rm' is aliased with the '-i' and '-v' option
# so it will default to safer operation, these option will make them
# verbose output of the operation and if an existing file is affected
# they will ask for confirmation.
#
alias cp="cp -iv"
alias mv="mv -iv"
alias rm="rm -iv"

# cd into the previous working directory by omitting `cd`.
alias ..="cd .."
alias ...="cd ../.."
alias ....="cd ../../.."

alias grep="grep --color=auto"
alias fgrep="fgrep --color=auto"
alias egrep="egrep --color=auto"

# flatpak commands
alias fli="flatpak install" # must be followed by a source, e.g. fli flathub
alias fliu="flatpak uninstall"
alias flls="flatpak list --app --columns='desc,app,orig'"
alias flu="flatpak update"

# xprop
alias getwmclass="xprop | grep -E '^(WM_CLASS|_NET_WM_NAME|WM_NAME)'"

# xmonad
alias xmrecomp="xmonad --recompile"

# bash history
alias hc="history -c"

# connect to camera hosted by android using ip camera
# the stream is directed to loopback device defined by v4l2loopback module
# so make sure the module is loaded before stream the camera
#
#alias gst-laucam="gst-launch-1.0 -t souphttpsrc location='http://192.168.43.1:8080/videofeed' do-timestamp=true is-live=true ! multipartdemux ! jpegdec ! videoconvert ! v4l2sink"

